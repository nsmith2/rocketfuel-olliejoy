var Cylon = require('cylon');

// 73acb814dc464a2c9a929c3daec91561 2B-8DE0 f087c1108de0 nathanael
// ebe03eac6e12417788b7936847de6b55 2B-7A89 ee33f0097a89 brob

Cylon.config({
    logging: {
        level: 'debug'
    }
});

console.log("Starting Cylon Robot...");
Cylon.robot({
    connections: {
        bluetooth: { adaptor: 'central', uuid: 'f087c1108de0', module: 'cylon-ble' }
    },

    devices: {
        ollie: { driver: 'ollie' }
    },

    display: function(err, data) {
        if (err) {
            console.log("Error:", err);
        } else {
            console.log("Data:", data);
        }
    },

    work: function(my) {

        // The following code will only output "Putting Ollie into dev mode." and "Sending anti-DoS string."
        my.ollie.devModeOn(function(err,data){
            console.log( "DevModeOn Callback Reached ");

            var rndColor, hexStr;
            setInterval(function() {
                rndColor = (Math.random()*(1<<24)|0);
                hexStr = "#"+("000"+rndColor.toString(16)).substr(-6)
                console.log('MAKE IT '+hexStr);
                my.ollie.setRGB(rndColor);
            },250);
        });

            //console.log("Make it GREEN");
            //my.ollie.setRGB(0x00FF00);
    }
}).start();