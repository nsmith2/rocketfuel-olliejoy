"use strict";
var util = require("util");
var Cylon = require("cylon");
var _ = require("lodash");

var config = __dirname + "/nyko.json";
var movement = {};
var tPos = 0;
var modeKey = '';
var speedKey = '';
var ollie;
var isMovementMode = false;
var movementIntervalId;

var timingInterval = 600;
var motorVal = 100;

// 73acb814dc464a2c9a929c3daec91561 2B-8DE0 f087c1108de0 nathanael
// ebe03eac6e12417788b7936847de6b55 2B-7A89 ee33f0097a89 brob

Cylon
    .robot()
    .connection("bluetooth", {  adaptor: 'central', uuid: 'f087c1108de0', module: 'cylon-ble' })
    .connection("joystick", { adaptor: "joystick" })
    .device("controller", { driver: "joystick", config: config, connection: "joystick" })
    .device("ollie", { driver: 'ollie', connection: 'bluetooth' })
    .on("ready", function(my) {

        ollie = my.ollie;

        movement = {
            lMode: my.ollie.MotorBrake,
            lSpeed: 0,
            rMode: my.ollie.MotorBrake,
            rSpeed: 0
        };

        console.log('robot ready');
        my.ollie.devModeOn(function(){

            console.log('ollie ready');
            my.ollie.setRGB(0xFF0000);


            my.controller.on("left_y:move", function(pos) {
                setMovement( pos, 'l' );
            });


            my.controller.on("right_y:move", function(pos) {
                setMovement( pos, 'r' );
            });

            // ALLOW STICK MOVEMENT
            my.controller.on("x:press",function(){
                console.log('Pressed X');
                turnOnMovementMode();
            });


            // STOP STICK MOVEMENT
            my.controller.on("circle:press",function(){
                console.log('Pressed Circle');
                turnOffMovementMode();
                after(500, function(){
                    console.log('setStabilization called.');
                    my.ollie.setStabilization();
                });
            });


            // SPIN OLLIE!
            my.controller.on("triangle:press", function(){
                console.log('Pressed Triangle');

                my.ollie.setRawMotorValues(
                    my.ollie.MotorForward, 200,
                    my.ollie.MotorReverse, 200
                );

                after(2000, function(){
                    my.ollie.setRawMotorValues(
                        my.ollie.MotorReverse, 200,
                        my.ollie.MotorForward, 200
                    );

                    after(2000, function(){
                        my.ollie.stop();
                        after(2000, function(){
                            console.log('setStabilization called.');
                            my.ollie.setStabilization();
                        })
                    });
                });
            });

            // CHANGE COLOR!
            my.controller.on("square:press", function() {
                var rndColor = (Math.random()*(1<<24)|0);
                var hexStr = "#"+("000"+rndColor.toString(16)).substr(-6)
                console.log('MAKE IT '+hexStr);
                my.ollie.setRGB(rndColor);
            });

            // ROLL!!
            my.controller.on('dpad_y:move', function(pos){
                console.log('Dpad Y: ' + pos);
                if(pos) {
                    pos = pos === -1 ? 0 : 180;
                    doDPadMove(pos);
                } else {

                }
            });

            my.controller.on('dpad_x:move', function(pos){
                console.log('Dpad X: ' + pos);
                if(pos) {
                    pos = pos === -1 ? 90 : 270;
                    doDPadMove(pos);
                }
            });
        });

    });

Cylon.start();

function setMovement( pos, dir) {

    tPos     = Math.round(pos*100)/100;
    modeKey  = dir + 'Mode';
    speedKey = dir + 'Speed';

    if ( tPos === 0 ) {
        movement[modeKey] = ollie.MotorBrake;
        movement[speedKey] = 0;
    } else {
        if (tPos < 0) {
            movement[modeKey] = ollie.MotorForward;
            movement[speedKey] = (tPos * -1).toScale(0,motorVal) | 0;
        } else if (tPos > 0) {
            movement[modeKey] = ollie.MotorReverse;
            movement[speedKey] = tPos.toScale(0,motorVal) | 0;
        }
    }

    logString();
}

function doMove() {
    console.log('doing movement...');
    ollie.setRawMotorValues(
        movement.lMode,
        movement.lSpeed,
        movement.rMode,
        movement.rSpeed
    );
}

function turnOnMovementMode() {
    console.log("Turn ON movement mode.");
    if(!isMovementMode) {
        movementIntervalId = setInterval(doMove,timingInterval);
        isMovementMode = true;
    }
}

function turnOffMovementMode() {
    console.log("Turn OFF movement mode.");
    if( isMovementMode ) {
        clearInterval(movementIntervalId);
        isMovementMode = false;
    }
}

var doDPadMove = _.throttle(function (pos) {
    console.log('DPad Ollie Move: ' + pos);
    ollie.roll( 80, pos, 1 );
}, timingInterval);


var ls = '';
function logString() {
    ls = '';

    if ( movement.lMode === 3 ) {
        ls += 'LEFT STOPPED ';
    } else {
        if ( movement.lMode === 1 ) {
            ls += 'LEFT FWD '
        } else if ( movement.lMode === 2 ) {
            ls += 'LEFT REV '
        }
    }
    ls += movement.lSpeed + ' | ';

    if ( movement.rMode === 3 ) {
        ls += 'RIGHT STOPPED ';
    } else {
        if ( movement.rMode === 1 ) {
            ls += 'RIGHT FWD '
        } else if ( movement.rMode === 2 ) {
            ls += 'RIGHT REV '
        }
    }
    ls += movement.rSpeed;
    console.log(ls);
}
